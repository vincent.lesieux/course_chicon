import Competitor
import Time
import tri

def read_competitors(text):
    """
    :param text: nom du fichier source
    :type text: string
    :return: un dictionnaire
    :rtype: dict
    :UC: le fichier existe
    """
    inscrits = dict([])
    with open(text, 'r') as entree:
        numero_brassard=0
        for ligne in entree:
            if (numero_brassard>0):
                inscrits[numero_brassard]=Competitor.create(ligne.rstrip('\n').split(";")[0], ligne.rstrip('\n').split(";")[1], ligne.rstrip('\n').split(";")[2], ligne.rstrip('\n').split(";")[3], numero_brassard)
            numero_brassard+=1
    return inscrits

def affichage(liste):
    for i in liste:
        print(Competitor.to_string(liste[i]))
        
affichage(read_competitors('mat_course_chicon/data/small_inscrits.csv'))
    
def select_competitor_by_bib(competiteurs,numero):
    try:
        return read_competitors('mat_course_chicon/data/small_inscrits.csv')[numero]
    except KeyError:
        print("Le numéro demandé n'est pas attribué")
    
#print(select_competitor_by_bib(read_competitors('mat_course_chicon/data/small_inscrits.csv'),212))

def select_competitor_by_birth_year(competiteurs,year):
    liste=[]
    for i in competiteurs:
        if competiteurs[i]['birth_date'].endswith(str(year)):
            liste.append(competiteurs[i])
    return liste

#print(select_competitor_by_birth_year(read_competitors('mat_course_chicon/data/small_inscrits.csv'),1980))

def read_performances(text):
    performances = dict([])
    with open(text, 'r') as entree:
        for ligne in entree:
            try:
                int(ligne.rstrip('\n').split(";")[0])==ligne.rstrip('\n').split(";")[0]
                performances[int(ligne.rstrip('\n').split(";")[0])]=Time.create(int(ligne.rstrip('\n').split(";")[1]),int(ligne.rstrip('\n').split(";")[2]),int(ligne.rstrip('\n').split(";")[3]))
            except:
                resultat=None
        return performances

#print(read_performances('mat_course_chicon/data/small_performances.csv'))

def set_performances(dic1,dic2):
    for i in dic2:
        try :
            dic2[i]['performance']=Time.to_string(dic1[i])
        except KeyError:
            resultat=None
    return dic2

#print(read_performances('mat_course_chicon/data/small_performances.csv'))
#print(read_competitors('mat_course_chicon/data/small_inscrits.csv'))

#print(set_performances(read_performances('mat_course_chicon/data/small_performances.csv'),read_competitors('mat_course_chicon/data/small_inscrits.csv')))

def sort_competitors_by_lastname(dic):
    new=dict([])
    liste=[]
    for i in dic:
        liste.append(dic[i]['last_name'])
    tri.tri_selection(liste,tri.compare_chaine_lexicographique)
    for i in range(len(liste)):
        for j in dic:
            if dic[j]['last_name']==liste[i]:
                new[j]=dic[j]
    return new
        
#print(sort_competitors_by_lastname(set_performances(read_performances('mat_course_chicon/data/small_performances.csv'),read_competitors('mat_course_chicon/data/small_inscrits.csv'))))

def conversion_en_seconde(text):
    try:
        heure=int(text.split(" ")[1].rstrip('h'))
        minute=int(text.split(" ")[2].rstrip('mn'))
        seconde=int(text.split(" ")[3].rstrip('s'))
        temps=3600*heure+60*minute+seconde
        return temps
    except AttributeError:
        return None
#       print("Pas de performance")
    
def sort_competitors_by_performance(dic):
    new=dict([])
    liste=[]
    for i in dic:
        if not (dic[i]['performance'] is None):
            liste.append(conversion_en_seconde(dic[i]['performance']))
        tri.tri_selection(liste,tri.compare_entier_croissant)
    for i in range(len(liste)):
        for j in dic:
            if conversion_en_seconde(dic[j]['performance'])==liste[i]:
                new[j]=dic[j]
                
    liste_sans_performance=[]
    for i in dic:
        if (dic[i]['performance'] is None):
            liste_sans_performance.append(dic[i]['last_name'])
    tri.tri_selection(liste_sans_performance,tri.compare_chaine_lexicographique)
    for i in range(len(liste_sans_performance)):
        for j in dic:
            if dic[j]['last_name']==liste_sans_performance[i]:
                new[j]=dic[j]                
    return new

#print(sort_competitors_by_performance(set_performances(read_performances('mat_course_chicon/data/small_performances.csv'),read_competitors('mat_course_chicon/data/small_inscrits.csv'))))


def print_results(dic):
    for i in dic:
        print("[{bib_num}]: {first_name} {last_name} ({sex} - {birth_date})    => {performance}".format(**dic[i]))

#par ordre de performance
#print_results(sort_competitors_by_performance(set_performances(read_performances('mat_course_chicon/data/small_performances.csv'),read_competitors('mat_course_chicon/data/small_inscrits.csv'))))

#par ordre alphabétique
#print_results(sort_competitors_by_lastname(set_performances(read_performances('mat_course_chicon/data/small_performances.csv'),read_competitors('mat_course_chicon/data/small_inscrits.csv'))))

def save_results(dic,text):
    sortie = open(text, 'w')
    sortie.writelines('Num_dossard;Prénom;Nom;Performance\n')
    for i in dic:
        if not (dic[i]['performance'] is None):
            sortie.writelines(str(dic[i]['bib_num'])+";"+dic[i]['first_name']+";"+dic[i]['last_name']+";"+dic[i]['performance']+"\n")
    sortie.close()

save_results(sort_competitors_by_performance(set_performances(read_performances('mat_course_chicon/data/small_performances.csv'),read_competitors('mat_course_chicon/data/small_inscrits.csv'))),"sauvegarde.csv")

def is_sexe_feminin(competitor):
    if competitor['sex']=="F":
        return True
    
def select_competitor(dic,predicat):
    liste=[]
    for i in dic:
        if predicat(dic[i]):
            liste.append(dic[i])
    return liste

#print(select_competitor(read_competitors('mat_course_chicon/data/small_inscrits.csv'),is_sexe_feminin))

def conversion(date_naissance):
    age=int(date_naissance.split("/")[2])+int(date_naissance.split("/")[1])/12+int(date_naissance.split("/")[0])/365
    return age

def is_plus_age(competitor1,competitor2):
    return conversion(competitor2['birth_date'])-conversion(competitor1['birth_date'])

def new_select_competitor_by_birth_year(dic,comp):
    for i in range(1,len(dic)):
        j=selection_min(dic,i)
        dic[i],dic[j]=dic[j],dic[i]
    return dic    
    
def selection_min(dic,i,comp):
    i_min=i
    for j in range(i+1,len(dic)):
        if comp(dic[j],dic[i_min])<0:
            i_min=j
    return i_min
